; TEMPCONV - a routine to deal with temperature conversion
    w !,"Call a tag, please",!,"Maybe try 'd main^tempconv'",!
    q ; quit if a specific tag is not called

main 
    d bootstrap ; Setup our working environment
    n tmp  ; New it up so previous value doesnt persist
    w !,"Welcome to RECTANGLE. Enter the input and output units and values for conversion checking"
    r !,"What is the input unit? ",tmp("in","unit"),!,"What is the input value? ",tmp("in","value")
    r !,"What is the output unit? ",tmp("out","unit"),!,"What is the output value? ",tmp("out","value")
    w !
    zwrite tmp
    q

area(length,width)  ; This is a tag that accepts parameters. 
                    ; Its not a function since it quits with no value.
    w !,"Area: ",length*width
    q ; Quit: return to the previous level of the stack.



bootstrap ; this bootstraps globals
    ; TODO - add locking here to fix race conditions
    k ^tcdata
    s ^tcdata("allowed_units",01)="f"
    s ^tcdata("allowed_units",02)="c"
    q ; Quit: return to the previous level of the stack.

perimeter(length,width)
    q 2*(length+width) ; Quits with a value; thus a function



validateunit(unit)
    n record
    k data
    i unit="" q 0
    s lev1=""
    s lev2=""
    f  s lev1=$o(^tcdata("allowed_units",lev1)) q:lev1=""  d
    . i ^tcdata("allowed_units",lev1)=unit d
    . . q 1
    q 0

; d validateunit^tempconv("f")
; w $$validateunit^tempconv("f")