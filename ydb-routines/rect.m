; RECTANGLE - a routine to deal with rectangle math
    w !,"Call a tag, please",!,"Maybe try 'd main^rect'",!
    q ; quit if a specific tag is not called

main 
    n length,width          ; New length and width so any previous value doesnt persist
    w !,"Welcome to RECTANGLE. Enter the dimensions of your rectangle."
    r !,"What is the length? ",length,!,"What is the width? ",width
    d area(length,width)            ;Do a tag
    s per=$$perimeter(length,width) ;Get the value of a function
    w !,"Perimeter: ",per,!
    ; log output to a global
    s ^counter=1+^counter
    s ^lastrect(^counter,"len")=length
    s ^lastrect(^counter,"wid")=width
    s lastrect(^counter,"len")=length
    s lastrect(^counter,"wid")=width
    w !,"Last few measurements",!
    zwr ^lastrect
    w !,!,"Last few measurements (not global)",!
    zwr lastrect
    q

area(length,width)  ; This is a tag that accepts parameters. 
                    ; Its not a function since it quits with no value.
    w !,"Area: ",length*width
    q ; Quit: return to the previous level of the stack.

perimeter(length,width)
    q 2*(length+width) ; Quits with a value; thus a function

