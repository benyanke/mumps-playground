#!/bin/bash

# Spins up a yottadb container on the first execution, and if run again, it shells into that existing container

export container_name="ydb"
export container_image="yottadb/yottadb"
#export container_image="yottadb/yottadb:r1.34"

running="$(docker ps --filter "name=${container_name}" --format "{{.ID}}" | wc -l | tr -d "[:space:]")"

# allow command override
if [[ "$@" == "" ]] ; then
  cmd="/opt/yottadb/current/ydb"
else
  cmd="$@"
fi

# Decide if we need to make a new container or connect to an existing one
if [[ "${running}" == "1" ]] ; then
  echo 
  echo "######################"
  echo "YottaDB instance already running, opening another session to it"
  echo "######################"
  echo
  docker exec -it "${container_name}" $cmd
else
  echo
  echo "######################"
  echo "Starting new container with image '${container_image}'"
  echo "######################"
  echo
  docker run \
    --name="${container_name}" \
    --rm -it -e ydb_routines="/routines" \
    -v $(pwd)/ydb-data:/data \
    -v $(pwd)/ydb-routines:/routines \
    ${container_image}
fi
