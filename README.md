# MUMPS Playground

A tinkering playground for M (MUMPS).

## Usage

To start a YottaDB instance, run `./run.sh` and you'll be dropped into a mumps prompt in a container.

```
./run.sh

######################
Starting new container with image 'yottadb/yottadb'
######################

YDB>
```

If you run `./run.sh` again, you will be `docker exec`'d into the container created by the first instance, so that you now have two sessions into the same DB instance.

## Routines

Put any mumps routines you wish to mount into the container in `/ydb-routines/*.m`. They will be mounted into the YottaDB instance by the shell script `./run.sh`.

To use the routine, simply `do` it based on the file name.

For example, to execute the routine stored at `/ydb-routines/rect.m` in the repo, you would run:

```mumps
YDB> do ^rect
```

Or to execute a tag within the routine, excecute in the typical MUMPS way.

```mumps
YDB> do main^rect
```

Note that if you modify them without restarting the database, you may need to `zlink "[routinename]"` at the MUMPS prompt. For example, if you edit `rect.m`, you should run `zlink "rect"` before trying to use the rect routine again.
